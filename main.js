/*------------------PARAMETRAGE----------------------*/
var express = require("express");

var app = express();
//Lier avec postman
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
//Lier avec le navigateur
app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*")
    res.setHeader("Access-Control-Allow-Headers", "*")
    next()
})

/*------------------INITIALISATION VARIABLES----------------------*/

//Initialisation variable liste de produits
let products = [];
//Initialisation des produits
let p1 = {
    id: 1,
    nom: "orange",
    price: 1.2,
    quantity: 5
}
let p2 = {
    id: 2,
    nom: "pomme",
    price: 2,
    quantity: 5
}
let p3 = {
    id: 3,
    nom: "banane",
    price: 1.5,
    quantity: 3
}
let p4 = {
    id: 4,
    nom: "abricot",
    price: 3,
    quantity: 7
}
//Ajout produits dans la liste de produits
products.push(p1,p2,p3,p4);
//console.log(produkts);

//Initialisation variable liste de paniers
let baskets = [];
//Initialisation des paniers
let b1={
    id:1,
    items: []
}
let b2={
    id:2,
    items: []
}
//Ajout paniers dans la liste de paniers
baskets.push(b1,b2);
//Ajout produits aux paniers
b1.items.push(p1,p2);
b2.items.push(p3,p4);
//console.log(b1);
//console.log(b2);

/*------------------REQUETES----------------------*/

//Récupérer tous les paniers
app.get("/baskets",(req,res)=>{
    if (baskets.length != 0) {//si il y a des paniers dans la liste
        res.status(200).send(baskets)//il renvoi le status 200 et les paniers
    }else{res.status(400).send("Bad request")}//sinon, il renvoi le status 400 d'erreur
});

//Récupérer un panier par son ID
app.get("/baskets/:id",(req,res)=>{
    let code = 400;//par défaut on n'a rien dans notre panier
    let content = "Bad request";
    baskets.forEach(basket=>{//pour chaque panier
        if (basket.id == req.params.id){//si le panier existe
            code = 200;//il affiche le code 200
            content = basket;//il affiche le contenu du panier
        }
    })
    res.status(code).send(content);//il envoi le code et contenu qui correspond
})

//Récupérer tous les produits
app.get("/products",(req,res)=>{
    if (products.length != 0) {//s'il il y a des produits, si la longueur est 0 c'est qu'il n'y a pas de produits
        res.status(200).send(products)//afficher le status 200 et les produits
    }else{res.status(404).send("Bad request")}//sinon afficher l'erreur et code 404
})

//Récupérer les produits d'un panier
app.get("/baskets/:id/products",(req,res)=>{
    let code = 404;
    let content = "Bad request";
    baskets.forEach(basket=>{ //pour chaque panier
        if (basket.id == req.params.id) {//si le panier demandé est dans la liste
            code = 200;//afficher le status code 200
            content = basket.items; //afficher les produits du panier
        }
    })  
    res.status(code).send(content);//afficher le status code et contenu/produits correspondants
})

//Ajouter un produit à un panier particulier
app.post("/baskets/:id/products",(req,res)=>{
    let code = 404;
    let content = "Bad request"
    baskets.forEach(basket=>{//pour chaque panier
        if (basket.id == req.params.id) {//si le panier existe
            basket.items.push(req.body)//on pousse le produit au panier au travers du body de postman
            code = 200;
            content = basket.items;
        }
    })
    res.status(code).send(content);
    
})
//Sur Postman on se met en POST
//On va dans BODY>X-WWW-FORM et on rajoute dans KEY le id, nom, price, quantity
//et dans VALUE les valeurs du produit
//On fait SEND et on aura notre nouveau produit ajouté

//Ajouter un produit à la liste des produits


//Supprimer un produit d'un panier
app.delete("baskets/:id")

//Supprimer un produit


app.listen(4444);


