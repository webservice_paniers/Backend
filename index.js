var express = require("express");

var app = express();

//Récupèrer les paniers
app.get("/baskets",(req,res)=>{ 
    res.send("toto")
    //Le panier s'est bien récupéré : 200
    //Si le panier a déjà été récupéré : 409
    //Le panier ne peut pas être récupéré : 500
    //OU Le panier ne s'est pas récuperé :
        //Méthode de transaction échouée : 424
        //Temps d'attente trop long : 522, 524, 504, 499
        //Service indisponible : 503, 409
        //Erreur du serveur : 500, 444
        //Pas de patiente du client : 429, 
        //Pas de paniers: 404
})

//Récupèrer les produits
app.get("/products",(req,res)=>{ 
    //Le produit s'est bien récupéré : 200
    //Si le panier a déjà été récupéré : 409
    //Le panier ne peut pas être récupéré : 500
    //Le produit ne s'est pas récuperé :
        //Méthode de transaction échouée : 424
        //Temps d'attente trop long : 522, 524, 504, 499
        //Service indisponible : 503, 409
        //Erreur du serveur : 500, 444
        //Pas de patiente du client : 429, 
        //Pas de paniers: 404
})

//Récupèrer un panier par son id
app.get("/baskets/:id",(req,res)=>{ 
    res.send("toto")
    //Le panier s'est bien récupéré : 200
    //Si le panier a déjà été récupéré : 409
    //Le panier ne peut pas être récupéré : 500
    //Le panier ne s'est pas récuperé :
        //Méthode de transaction échouée : 424
        //Temps d'attente trop long : 522, 524, 504, 499
        //Service indisponible : 503, 409
        //Erreur du serveur : 500, 444
        //Pas de patiente du client : 429, 
        //Pas de paniers: 404
})

//Récupèrer tous les produits d'un panier
app.get("/baskets/:id/products",(req,res)=>{ 
    res.send("toto")
    //Le panier s'est bien récupéré : 200
    //Si le panier a déjà été récupéré : 409
    //Le panier ne peut pas être récupéré : 500
    //Le panier ne s'est pas récuperé :
        //Méthode de transaction échouée : 424
        //Temps d'attente trop long : 522, 524, 504, 499
        //Service indisponible : 503, 409
        //Erreur du serveur : 500, 444
        //Pas de patiente du client : 429, 
        //Pas de paniers: 404
})

//Supprimer un produit
app.delete("/products/:id",(req,res)=>{
    res.send("toto")
    //Si le produit est bien supprimé : 200
    //Si le produit n'est pas supprimé
        //Il a déjà été supprimé : 409 ou 418 (conflit, la ressource n'existe pas)
        //Produit ne peut pas être supprimé : 500
        //Temps d'attente trop long : 522, 524, 504, 499
        //Service indisponible : 503, 409
        //Erreur du serveur : 500, 444
        //Pas de patiente du client : 429, 
})

//Supprimer un produit d'un panier
app.delete("/baskets/:id/products",(req,res)=>{
    res.send("toto")
    //Si le produit est bien supprimé : 200
    //Si le produit n'est pas supprimé
        //Il a déjà été supprimé : 409 ou 418 (conflit, la ressource n'existe pas)
        //Produit ne peut pas être supprimé : 500
        //Temps d'attente trop long : 522, 524, 504, 499
        //Service indisponible : 503, 409
        //Erreur du serveur : 500, 444
        //Pas de patiente du client : 429,
})

//Ajouter un produit à un panier particulier
app.post("/baskets/:id/products",(req,res)=>{
    baskets.push("toto")
    res.send("toto")
    //Le produit s'est bien ajouté : 200
    //Le produit ne s'est pas bien ajouté :
        //Méthode de transaction échouée : 424
        //Temps d'attente trop long : 522, 524, 504, 499
        //Service indisponible : 503, 409
        //Erreur du serveur : 500, 444
        //Pas de patiente du client : 429, 
        //Pas de paniers: 404
})

//Ajouter un produit à une liste de produits
app.post("/baskets/:id/products",(req,res)=>{
    products.push("toto")
    res.send("toto")
    //Le produit s'est bien ajouté : 200
    //Le produit ne s'est pas bien ajouté :
        //Méthode de transaction échouée : 424
        //Temps d'attente trop long : 522, 524, 504, 499
        //Service indisponible : 503, 409
        //Erreur du serveur : 500, 444
        //Pas de patiente du client : 429, 
        //Pas de paniers: 404
})

//Afficher tous les paniers validés par le client

/*app.upload("/baskets",(req,res)=>{
    res.send("toto")
    //Le produit s'est bien ajouté : 200
    //Le produit ne s'est pas bien ajouté :
        //Méthode de transaction échouée : 424
        //Temps d'attente trop long : 522, 524, 504, 499
        //Service indisponible : 503, 409
        //Erreur du serveur : 500, 444
        //Pas de patiente du client : 429, 
        //Pas de paniers: 404
})*/

app.listen(4444);


