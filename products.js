var express = require("express");

var app = express();

//products
/*{
    id : 1,
    designation : "poire",
    price : number,
    quantity : number
}

//baskets
{
    id : number,
    items : product[],
    total: number
}*/

//initialisation variable products
var products = [];
//Initialiser des produits
let p1 = {
    id:1,
    designation: "orange",
    price: 3.50,
    quantity:1
}
let p2 = {
    id:2,
    designation: "pain",
    price: 3,
    quantity:3
}
//Ajouter des produits à notre liste de products
products.push(p1,p2)

/* Autre méthode
products.push({
    id: 1,
    designation: "pomme",
    price: 3,
    quantity: 5
},
{
    id: 2,
    designation: "poire",
    price: 2,
    quantity: 5
},
{
    id: 3,
    designation: "orange",
    price: 2.5,
    quantity: 6
});*/

//initialisation variable baskets
var baskets = [];
//Initialiser des paniers
let basket1 = {
    id:1,
    items: []
}

let basket2 = {
    id:2,
    items:[]
}
//Ajouter les paniers à la liste des paniers
baskets.push(basket1,basket2)
//Ajouter les produits aux paniers, dans Items
basket1.items.push(products[0])
basket2.items.push(products)

//récupérer produit qu'on a dans la liste des produits et le mettre dans le panier
/*baskets.push({
    id : 0,
    items : [],
    total: 10
},
{
    id : 1,
    items : [],
    total: 15
});*/
//Initialisation panier 1 qui ira dans la liste des paniers
/*var basket1 = [];
//Je rentre les produits dans le panier 1 en cherchant son id
basket1.push(products.find(produit => produit.id === 1),products.find(produit => produit.id === 2));
//Je rentre le panier 1 dans la liste des paniers
baskets.push(basket1);
//Initialisation panier 1 qui ira dans la liste des paniers
var basket2 = [];
//Je rentre les produits dans le panier 1 en cherchant son id
basket2.push(products.find(produit => produit.id === 1),products.find(produit => produit.id === 3));
//Je rentre le panier 1 dans la liste des paniers
baskets.push(basket2);*/

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "")
    res.setHeader("Access-Control-Allow-Headers", "")
    next()
})

//Récupérer tous les paniers
app.get("/baskets",(req,res)=>{ 
    res.send(baskets)
});

//Récupérer un panier par son ID
app.get("/baskets/:id",(req,res)=>{
    for (let i = 0; i < baskets.length; i++) {
        if(baskets[i].id == req.params.id){
        var panier = baskets[i];
        res.send(panier)
        }
    }
});

/*/récupérer le panier par son ID - Version Alex avec FOREACH
app.get('/baskets/:id', function (req, res) {
    baskets.forEach(element => {
    //si existe, affiche
    if(element.id == req.params.id){
    res.send(element)
    return;
    }
});
res.status(404).send('L\'id n\'existe pas')
});*/

//Récupérer tous les produits
app.get("/products/:id",(req,res)=>{
    for (let i = 0; i < products.length; i++) {
        if(products[i].id == req.params.id){//id produit = id produit demandé, tu me crées une nouvelle variable
            let myproducts = products[i];//la nouvelle variable qui montrera le produit selon l'id sélectionné dans le naviateur
            res.send(myproducts)//renvoie la réponse à notre requête
            console.log(myproducts); 
        }   
    }    
});


/*
//Récupérer un produit d'un panier particulier CORRIGER - VALENTIN

function sendResponse(code, content, res){
    res.status(code).send(content);
}



app.get("/baskets/:id/products",(req,res)=>{
    let code = 404;
    let content = "Aucune ressource";
    baskets.forEach(basket=>{
        if (basket.id == req.params.id) {
            if(basket.products.length == 0){
                code = 200;
                content = "Ce panier est vide";
            }   else {
                    code=200;
                    content = baskets.products;
                }
        res.send(code,content,res)
        }
    })
    //sendResponse (code, content, res);
});*/

//AJouter un produit à u panier particulier
//Ajouter un produit à la liste des produits

/*
//Supprimer un produit d'un panier - VALENTIN
//La requête doit envoyer un param au format "idp=id" du produit à supprimer
//idp = id produit
//idb = id basket
app.delete("/baskets/:id",(req,res)=>{
    let code = 400;
    let content = "Mauvaise requête";

    let idp = req.body.idp; //id du produit à supprimer du panier
    let idb = req.params.id; //id du panier

    baskets.forEach(basket=>{
        if(basket.id == idb){
            basket.products.forEach(product=>{
                if(product.id == idp){
                    basket.products.pop(product);//pop contraire a pull, ça supprime
                    code = 200;
                    content = "Le produit a bien été supprimé du panier" + basket.id;
                }
            })
        }
    })

    sendResponse (code,content, res);
})

//Supprimer un produit

//Afficher tous les paniers validés d'un client - YOHAN
var basket3={
    id:3,
    items:[],
    refClient:1,
    valide:true
}
baskets.push(basket3)
basket3.items.push(products[1])

var client ={
    id:1,
    nom: "remy"
}

app.get("/client/:id",(req,res)=>{
    baskets.forEach(element=>{
        if(element.refClient == req.params.id && element.valide){
            res.send(element)
        }
    })
})*/


app.listen(4444);

/*
//V2. YOHAN

var express = require('express')
var app = express()

const products = []
const baskets = []

let basket = {
    id:12,
    items: []
}

let basket1 = {
    id:13,
    items:[]
}

let p1 = {
    id:1,
    designation: "pain au chocolat",
    price: 3.50,
    quantity:1
}
let p2 = {
    id:2,
    designation: "croissant au chocolat",
    price: 3,
    quantity:3
}
*//*
products.push(p1,p2)
baskets.push(basket,basket1)
basket.items.push(products[0])
basket1.items.push(products)


app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "")
    res.setHeader("Access-Control-Allow-Headers", "")
    next()
})*/
/*
//nous renvois la liste des produits disponnibles

app.get("/products",(req,res) =>{
    console.log(products);
    res.send(products)
})

//nous renvois un panier
app.get("/basket1",(req,res) =>{
    res.send(basket)
})

app.get("/basket2",(req,res)=> {
    res.send(basket1)
})

app.get("/basket",(req,res)=> {
    res.send("ok")
})*/

//nous renvois un panier par son ID
//MARCHE PAS
/*app.get("/baskets:id/products",(req,res)=> {
        baskets.forEach(element => {
            if(req.params.id == ":"+element.id){
                res.send(element)
            }
        });
    })*/

/*app.get("/baskets:id/products",(req,res)=> {
    console.log(req.params.id);
    if(req.params.id == ":"+baskets[0].id || ":"+baskets[1].id ){
        baskets.forEach(element => {
            if(req.params.id == ":"+element.id){
                res.send(element)
            }
        });
    }else {
        res.send("nop")
    }


    // if(req.params.id == ":12"){
    //     res.send(basket.items)
    // }else if (req.params.id == ":13"){
    //     res.send(basket1.items)
    // } else {
    //     res.send("panier non trouvé ....")
    // }
})

app.get("/basket/product",(req,res)=> {

    res.send(basket.items)
})

app.get("/basket1/product",(req,res)=>{
    res.send(basket1.items)
})





app.listen(4444)*/